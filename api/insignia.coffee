"use strict"

Hope      = require("zenserver").Hope
Auth      = require "../common/authentication"
Insignia  = require "../common/models/insignia"
C         = require "../common/constants"

module.exports = (server) ->
  ###
   * Register a insignia
   * @method  POST
   * @param  {String} Title of inisgnia
   * @param  {String} Description of inisgnia
   * @return {Object} Insignia details
  ###
  server.post "/api/insignia", (request, response) ->
    if request.required ["category", "title"]
      Hope.shield([ ->
        Auth request, response
      , (error, session) ->
        request.parameters["user"] = session._id
        Insignia.register request.parameters
      ]).then (error, result) ->
        if error
          response.json message: error.message, error.code
        else
          response.json result.parse()

  server.put "/api/insignia/:id", (request, response) ->
    if request.required ["id"]
      Hope.shield([ ->
        Auth request, response
      , (error, session) ->
        parameters = {}
        available = ["category", "title", "description", "airline"]
        for key in available when request.parameters[key]
          parameters[key] = request.parameters[key]
        filter = _id : request.parameters.id
        filter["user"] = session._id if session.role isnt C.ROLE.ADMIN
        Insignia.updateAttributes filter, parameters
      ]).then (error, result) ->
        if error
          response.json message: error.message, error.code
        else
          response.json result.parse()
