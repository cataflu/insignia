"use strict"

Hope  = require("zenserver").Hope
Auth  = require "../common/authentication"
User  = require "../common/models/user"

module.exports = (server) ->
  ###
   * Retrieve user profile details
   * @method  GET
   * @param  {String} id: user ID
   * @return {Object} User profile details
  ###
  server.get "/api/user/:id", (request, response) ->
    Hope.shield([ ->
      Auth request, response
    , (error, session) ->
      User.search _id: request.parameters.id, limit = 1
    ]).then (error, result) ->
      if error
        response.json message: error.message, error.code
      else
        response.json result.parse request.headers.host
