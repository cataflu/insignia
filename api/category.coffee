"use strict"

Hope      = require("zenserver").Hope
Auth      = require "../common/authentication"
Category  = require "../common/models/category"
C         = require "../common/constants"

module.exports = (server) ->
  ###
   * Get all categories
   * @method  GET
   * @return {Array} Categories details
  ###
  server.get "/api/categories", (request, response) ->
    Category.search().then (error, categories) ->
      if error
        response.json message: error.message, error.code
      else
        response.json (category.parse() for category in categories)

  ###
   * Register category
   * @method  POST
   * @param  {String} Title of category
   * @param  {String} Description of category
   * @return {Object} Category details
  ###
  server.post "/api/category", (request, response) ->
    if request.required ["title"]
      Hope.shield([ ->
        Auth request, response, role: C.ROLE.ADMIN
      , (error, session) ->
        Category.register request.parameters
      ]).then (error, result) ->
        if error
          response.json message: error.message, error.code
        else
          response.json result.parse()

  ###
   * Update category by ADMIN
   * @method  PUT
   * @param  {String} Title of category
   * @param  {String} Description of category
   * @return {Object} Category details
  ###
  server.put "/api/category", (request, response) ->
    if request.required ["id"]
      Hope.shield([ ->
        Auth request, response, role: C.ROLE.ADMIN
      , (error, session) ->
        Category.updateAttributes request.parameters.id, request.parameters
      ]).then (error, result) ->
        if error
          response.json message: error.message, error.code
        else
          response.json result.parse()
