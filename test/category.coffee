"use strict"

Test = require("zenrequest").Test

module.exports = ->
  tasks = []
  tasks.push _register ZENrequest.USERS[0], category for category in ZENrequest.CATEGORIES
  tasks.push _get ZENrequest.USERS[1]
  tasks.push _update ZENrequest.USERS[0], ZENrequest.CATEGORIES[0]
  tasks

# PROMISES ---------------------------------------------------------------------
_register = (session, category) -> ->
  message = "[CATEGORY] #{session.mail} CREATES #{category.title}"
  Test "POST", "api/category", category, _session(session), message, 200, (response) ->
    category.id = response.id

_update = (session, category) -> ->
  message = "[CATEGORY] #{session.mail} UPDATES category data"
  data =
    id          : category.id
    title       : "#{category.title} + #{new Date()}"
    description : "#{category.description} + #{new Date()}"
  Test "PUT", "api/category", data, _session(session), message, 200

_get = (session, category) -> ->
  message = "[CATEGORY] #{session.mail} GETS all categories"
  Test "GET", "api/categories", null, _session(session), message, 200

# -- Private methods -----------------------------------------------------------
_session = (user = null) ->
  if user?.token? then authorization: user.token else null
