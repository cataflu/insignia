"use strict"

Test = require("zenrequest").Test

module.exports = ->
  tasks = []
  tasks.push _getUserDetails ZENrequest.USERS[1], ZENrequest.USERS[0]
  tasks

# PROMISES ---------------------------------------------------------------------
_getUserDetails = (session, user) -> ->
  message = "[USER]     #{session.mail} GETS details of #{user.mail}"
  Test "GET", "api/user/#{user.id}", null, _session(session), message, 200

# -- Private methods -----------------------------------------------------------
_session = (user = null) ->
  if user?.token? then authorization: user.token else null
