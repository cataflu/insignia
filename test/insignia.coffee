"use strict"

Test = require("zenrequest").Test

module.exports = ->
  tasks = []
  for i in [0..1]
    tasks.push _register ZENrequest.USERS[i], ZENrequest.CATEGORIES[i], ZENrequest.INSIGNIAS[i]
  tasks.push _update ZENrequest.USERS[0], ZENrequest.INSIGNIAS[0]
  tasks.push _update ZENrequest.USERS[0], ZENrequest.INSIGNIAS[1]

# ERROR ------------------------------------------------------------------------
  tasks.push _update ZENrequest.USERS[2], ZENrequest.INSIGNIAS[1], unauthorized = true
  tasks

# PROMISES ---------------------------------------------------------------------
_register = (session, category, insignia) -> ->
  message = "[INSIGNIA] #{session.mail} CREATES #{insignia.title}"
  insignia.category = category.id
  Test "POST", "api/insignia", insignia, _session(session), message, 200, (response) ->
    insignia.id = response.id

_update = (session, insignia, unauthorized = null) -> ->
  message = "[INSIGNIA] #{session.mail} UPDATES #{insignia.title}"
  code = 200
  if unauthorized
    message = "[INSIGNIA] #{session.mail} is unauthorized to update #{insignia.title}"
    code = 402
  insignia.title = "#{insignia.title} #{new Date()}"
  insignia.description = "#{insignia.description} #{new Date()}"
  insignia.airline = 1
  Test "PUT", "api/insignia/#{insignia.id}", insignia, _session(session), message, code

# -- Private methods -----------------------------------------------------------
_session = (user = null) ->
  if user?.token? then authorization: user.token else null
