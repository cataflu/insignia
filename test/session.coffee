"use strict"

Test = require("zenrequest").Test

module.exports = ->
  tasks = []
  tasks.push _signup user for user in ZENrequest.USERS
  tasks.push _login user for user in ZENrequest.USERS
  tasks.push _update ZENrequest.USERS[0]
  tasks.push _get ZENrequest.USERS[0]

# ERROR ------------------------------------------------------------------------
  tasks.push _signup()
  tasks.push _login ZENrequest.USERS[0], bad_credentials: true
  tasks

# PROMISES ---------------------------------------------------------------------
_signup = (user) -> ->
  if user
    message = "[SIGNUP]   #{user.mail}"
    code    = 409
  else
    message = "[SIGNUP]   Missing parameters"
    code    = 400
  Test "POST", "api/signup", user, null, message, code

_login = (user, bad_credentials) -> ->
  if bad_credentials
    user    = mail: user.mail, password: "xxxx"
    message = "[LOGIN]    Incorrect username or password."
    code    = 401
  else
    message = "[LOGIN]    #{user.mail}"
    code    = 200
  Test "POST", "api/login", user, null, message, code, (response) ->
    unless bad_credentials
      user.id = response.id
      user.token = response.token

_get = (session) -> ->
  message = "[PROFILE]  #{session.mail} gets profile data"
  Test "GET", "api/profile", null, _session(session), message, 200

_update = (session) -> ->
  message = "[PROFILE]  #{session.mail} UPDATES profile data"
  data = username: "#{session.username} #{new Date()}", location: "Perth"
  Test "PUT", "api/profile", data, _session(session), message, 200

# -- Private methods -----------------------------------------------------------
_session = (user = null) ->
  if user?.token? then authorization: user.token else null
