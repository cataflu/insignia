"use strict"

Hope      = require("zenserver").Hope
Schema    = require("zenserver").Mongoose.Schema
db        = require("zenserver").Mongo.connections.primary

Category = new Schema
  title       : type: String, unique: true, trim: true
  description : type: String
  created_at  : type: Date, default: Date.now

# -- Static methods ------------------------------------------------------------
Category.statics.register = (values) ->
  promise = new Hope.Promise()
  category = db.model "Category", Category
  new category(values).save (error, value) ->
    if error?.code is 11000
      error = code: 400, message: "Category already existis"
    promise.done error, value
  promise

Category.statics.search = (query = {}, limit = 0) ->
  promise = new Hope.Promise()
  @find(query).limit(limit).exec (error, value) ->
    if limit is 1 and not error
      error = code: 402, message: "Category not found." if value.length is 0
      value = value[0]
    promise.done error, value
  promise

Category.statics.updateAttributes = (id, attributes) ->
  promise = new Hope.Promise()
  @findByIdAndUpdate id, attributes, new: true,  (error, value) ->
    promise.done error, value
  promise

# -- Instance methods ----------------------------------------------------------
Category.methods.parse = ->
  id          : @_id
  title       : @title
  description : @description
  created_at  : @created_at

exports = module.exports = db.model "Category", Category
