"use strict"

Hope      = require("zenserver").Hope
Schema    = require("zenserver").Mongoose.Schema
db        = require("zenserver").Mongo.connections.primary
PassHash  = require "password-hash"
C         = require "../constants"

User = new Schema
  role        : type: Number, default : C.ROLE.DEFAULT
  username    : type: String, trim    : true, unique: true
  mail        : type: String, unique  : true
  image       : type: String, default : "image.png"
  token       : type: String
  password    : type: String
  location    : type: String
  created_at  : type: Date, default: Date.now
  updated_at  : type: Date

# -- Static methods ------------------------------------------------------------
User.statics.signup = (values) ->
  promise = new Hope.Promise()
  @findOne(mail: values.mail).exec (error, value) ->
    return promise.done code: 409, message: "Mail already in use" if value?
    user = db.model "User", User
    values.password = PassHash.generate values.password
    new user(values).save (error, value) -> promise.done error, value
  promise

User.statics.login = (values) ->
  promise = new Hope.Promise()
  @findOne mail: values.mail, (error, user) ->
    if user is null or not PassHash.verify values.password, user.password
      error = code: 401, message: "Incorrect username or password."
      promise.done error
    else
      promise.done error, user
  promise

User.statics.search = (query, limit = 0) ->
  promise = new Hope.Promise()
  @find(query).limit(limit).exec (error, value) ->
    if limit is 1 and not error
      error = code: 402, message: "User not found." if value.length is 0
      value = value[0]
    promise.done error, value
  promise

# -- Instance methods ----------------------------------------------------------
User.methods.updateAttributes = (attributes) ->
  promise = new Hope.Promise()
  @[key] = value for key, value of attributes
  @save (error, result) ->
    if error?.code is 11000
      error = code: 400, message: "Username has already been taken"
    promise.done error, result
  promise

User.methods.parse = (host) ->
  id          : @_id
  username    : @username
  mail        : @mail
  image       : "http://#{host}/assets/user/" +  @image
  token       : @token
  location    : @location
  created_at  : @created_at

exports = module.exports = db.model "User", User
