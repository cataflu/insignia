"use strict"

Hope      = require("zenserver").Hope
Schema    = require("zenserver").Mongoose.Schema
db        = require("zenserver").Mongo.connections.primary

Insignia = new Schema
  user        : type: Schema.ObjectId, ref: "User"
  category    : type: Schema.ObjectId, ref: "Category"
  title       : type: String
  description : type: String
  image       : type: String, default: "image.png"
  airline     : type: Number
  updated_at  : type: Date
  created_at  : type: Date, default: Date.now

# -- Static methods ------------------------------------------------------------
Insignia.statics.register = (values) ->
  promise = new Hope.Promise()
  insignia = db.model "Insignia", Insignia
  new insignia(values).save (error, value) -> promise.done error, value
  promise

Insignia.statics.search = (query = {}, limit = 0) ->
  promise = new Hope.Promise()
  @find(query).limit(limit).exec (error, value) ->
    if limit is 1 and not error
      error = code: 402, message: "Insignia not found." if value.length is 0
      value = value[0]
    promise.done error, value
  promise

Insignia.statics.updateAttributes = (query, attributes) ->
  promise = new Hope.Promise()
  attributes.updated_at = new Date()
  @findOneAndUpdate query, attributes, new: true,  (error, value) ->
    error = code: 402, message: "Insignia not found" if error or value is null
    promise.done error, value
  promise

# -- Instance methods ----------------------------------------------------------
Insignia.methods.parse = ->
  id          : @_id
  user        : @user
  category    : @category
  title       : @title
  description : @description
  image       : @image
  airline     : @airline
  updated_at  : @updated_at
  created_at  : @created_at

exports = module.exports = db.model "Insignia", Insignia
