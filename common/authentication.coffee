"use strict"

Hope = require("zenserver").Hope
jwt  = require "jwt-simple"
User = require "./models/user"

module.exports = (request, response, query = null, redirect = false) ->
  promise = new Hope.Promise()
  if request.session
    session = jwt.decode request.session, ZEN.token
    if session.expire > new Date()
      filter = query or {}
      filter["_id"] = session.id
      User.search(filter, limit = 1).then (error, user) ->
        unless user?
          if redirect then promise.done true
          do response.unauthorized
        else
          promise.done error, user
    else
      if redirect then promise.done undefined, undefined
      response.json message: "Session expired. Please log in again", 401
  promise



