module.exports =

  ROLE:
    DEFAULT: 0
    ADMIN  : 1

  AIRLINE:
    "QANTAS"        : 0
    "AIRLINK"       : 1
    "AIRNORTH"      : 2
    "AIR AUSTRALIA" : 3
    "US AIR FORCE"  : 4
